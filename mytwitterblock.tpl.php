<?php
drupal_add_js(drupal_get_path('module', 'my_twitter_block') . '/mytweets.js', array('group' => JS_THEME));
?>
    
    
<div class="icon-author animated flipInX">
  <div class="bird"></div>
  <p class="twitter-author"><a href="http://twitter.com/<?php print variable_get('twitter_login_username'); ?>" target="_blank">Follow us on Twitter</a></p>
</div>
<div class="space"></div>
<!-- Twitter Slider -->       
<div class="twitter-slider">              
<div id="twitter-feed"></div>  
</div>

<script type="text/javascript">
    $('#twitter-feed').twittie({
        dateFormat: '%b. %d, %Y',
        template: '<a target="_blank" href="{{url}}">{{date}}</a>, {{tweet}}',
        count: 5,
		
    });
		
		
		
	
	jQuery(window).load(function(){   

	  jQuery(document).ready(function($){ 
	  
	/*----------------------------------------------------*/
	/* TWITTER CALLBACK FUNCTION
	/*----------------------------------------------------*/
	  if ($('#twitter-feed').length) {		
		  $('#twitter-feed').find('ul').addClass('slides');
		   $('#twitter-feed').find('ul li').addClass('slide');
		  $('#twitter-feed').contents().wrapAll('<div class="flexslider">');
	  };
	  
	 
	
	  });
	  
 
	  $('.twitter-slider .flexslider').flexslider({						
		animation: "slide",
		slideshow: true,
		slideshowSpeed: 5000,
		animationDuration: 1000,
		directionNav: true,
		controlNav: true,
	    loop: true,
		smootheHeight:true,
		after: function(slider) {
		  slider.removeClass('loading');
		}
		
		
	});
	  
});
</script>


